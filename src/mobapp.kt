fun main() {

    var f1: Point = Point()
    f1.x = 7.0F
    f1.y = 8.0F

    var f2: Point = Point ()
    f2.x = 7.0F
    f2.y = 8.0F

    println (f1.equals(f2))
    println (f1.toString ())
    println (f1.xtoMinus())
    println (f1.ytoMinus())
}


class Point {

    var x: Float = 0.0F
    var y: Float = 0.0F
    override fun equals(other: Any?): Boolean {
        if (other is Point){
            if (x * other.y / y == other.x){
                return true
            }
        }
        return false
    }

    override fun toString (): String {
        return "$x , $y"
    }
    fun xtoMinus () : Float {
        return -x
    }
    fun ytoMinus () : Float {
        return -y
    }

}